---
id: release-2-1-0
title: PeerTube 2.1 is out!
date: February 12, 2020
twitter: https://twitter.com/framasoft/status/1227937130531479552
---

Hi everybody,

A few months after PeerTube V2 announcement, we are pleased to announce you that version 2.1 has been released. Here's a quick overview of the features it brings…

#### An even more pleasant interface

We are continually striving to improve PeerTube's interface by collecting users opinions so that we know what is causing them trouble (in terms of understanding and usability for example). Firstly, even if it's no big deal, we have added a small animation on the videos when you play and pause them. We really like this little development! We also worked on the different icons graphics on the video viewing page. The icons and buttons are now more refined and you can also see the icons _I like / I don't like_ without being connected. It provides users to understand how they can interact with the video.

![](/img/news/release-2.1/en/icons-ui.png)

When you are logged in and put your mouse on videos' thumbnails, a small clock-shaped icon appears in the thumbnail's upper right corner to display Watch Later. Click it to add the video to your Watch Later playlist.

![](/img/news/release-2.1/en/watch-later.png)

We also improved the contents' layout of PeerTube presentation page located in About category / PeerTube tab. On this page, you now have easy access to PeerTube documentation, to the various apps available and to the PeerTube Contribution Guide.

![](/img/news/release-2.1/en/about.png)

#### An enhanced documentation

We are aware that documenting software allows more people to use it. That's why we added many contents to the [PeerTube documentation](https://docs.joinpeertube.org). PeerTube's administrators can now find information on how to use remote storage, how to manage videos' redundancy between instances and how to use PeerTube logs to understand what happens on their instances. We also added information on the various available features to customize an instance's interface, on moderation tools and how to mute instances or accounts.

Because providing a video player easily integrated into any web environment is essential for us, PeerTube now provides a library for developers to control the integration of a video player via the [PeerTube Embed API](https://docs.joinpeertube.org/#/api-embed-player).

#### Developments on comments

Interactivity with users is one of the recipes to success on videos platforms. Interacting with Internet users and responding to comments helps content producers to build an audience. That's why this latest version includes enhancements that help video makers to interact more with their viewers.

First, the comments' graphic layout has been modified: it is now easier to find your way between original comments and answers. Avatars' visuals placed next to each user name have been improved and the display name (and its identifier) are now more readable. When a video maker responds to a comment on one of his videos, it is easier to identify him or her because his name is highlighted.

![](/img/news/release-2.1/en/comments-ui.png)

It is now possible to display comments according to 2 criteria: the most recent first (default display) or most replies first.

![](/img/news/release-2.1/en/comments-sort.png)

Another news: you can now write comments in Markdown language in a restricted syntax.

Finally, we added an <em>Options</em> menu underneath users' comments to mute an account or an instance very easily.

![](/img/news/release-2.1/en/comments-options.png)

#### More features

As some of you asked us, we added a new privacy mode. You can now choose to broadcast a video in __internal__ mode: the video is available only for connected users to the instance on which the video is uploaded. This feature allows a video to be watched only by a group of friends, family or work.

PeerTube now automatically generates hyperlinks when a time code is mentioned in the video description or comments. Handy for mentioning a part of the video or making a chapter in the description!

![](/img/news/release-2.1/en/timecode.png)

Finally, the collaborative translation of PeerTube's contents is now done using <a href="https://weblate.framasoft.org/projects/peertube/" target="_blank" rel="noreferrer noopener">Weblate</a>, which is much more powerful and pleasant to use than the tool we we're using until now (Zanata).

This new release includes many other improvements. You can see the complete list on <a href="https://github.com/Chocobozzz/PeerTube/releases/tag/v2.1.0" target="_blank" rel="noopener noreferrer">https://github.com/Chocobozzz/PeerTube/releases/tag/v2.1.0</a>

Thanks to all PeerTube contributors!
Framasoft
