export default {
  data: function () {
    return {
      contentSelectionsEN: [
        {
          type: 'video',
          title: 'Nothing to hide',
          thumbnailName: 'nothing-to-hide.jpg',
          url: 'https://media.zat.im/videos/watch/35badfed-5322-48ac-b5c1-71b1ad88262e',
          tags: [ '#privacy', '#documentary' ],
          description: 'Nothing to hide (2017) is a Franco-German feature-length documentary by Marc Meillassoux and Mihaela Gladovic, about how mass surveillance affects individuals and society. Taking a critical look at the laws allowing State surveillance that were implemented by several countries in the past few years, we are reminded of how important the debate about usage of personal data is and how it questions the very basis of democracy.'
        },

        {
          type: 'video',
          title: 'The Internet\'s Own Boy: The Story of Aaron Schwartz',
          thumbnailName: 'internet-own-boy.jpg',
          description: 'A biopic about the story of programming prodigy and information activist Aaron Swartz. From Swartz\'s help in the development of the basic internet protocol RSS to his co-founding of Reddit, his fingerprints are all over the internet. But it was Swartz\'s groundbreaking work in social justice and political organizing combined with his aggressive approach to information access that ensnared him in a two year legal nightmare. It was a battle that ended with the taking of his own life at the age of 26. This film is a personal story about what we lose when we are tone deaf about technology and its relationship to our civil liberties.',
          tags: [ '#activism', '#freedom', '#Reddit' ],
          url: 'https://vidcommons.org/videos/watch/9952af68-0b30-4ea0-81ac-a26444541331'
        },

        {
          type: 'video',
          title: 'Sita Sings the Blues',
          thumbnailName: 'sita-sings-blues.jpg',
          description: 'Written, directed, produced and animated by American artist Nina Paley Sita Sings the Blues is an animated movie where Sita is a goddess separated from her beloved Lord and husband Rama. Three hilarious shadow puppets narrate both ancient tragedy and modern comedy in this beautifully animated interpretation of the Ramayana. Set to the 1920\'s jazz vocals of torch singer Annette Hanshaw, Sita Sings the Blues earns its tagline as "the Greatest Break-Up Story Ever Told."',
          tags: [ '#Ramayana', '#musical', '#fantasy' ],
          url: 'https://open.tube/videos/watch/a5ec9a36-36dc-4f7f-9743-702b02709ba5'
        },

        {
          type: 'instance',
          title: 'XR Tube',
          thumbnailName: 'extinction-rebellion.jpg',
          description: ' XR Tube is eXtinction Rebellion\'s instance. You can find on this instance many videos documenting the actions of this global environmental movement with the stated aim of using nonviolent civil disobedience to compel government action to avoid tipping points in the climate system, biodiversity loss, and the risk of social and ecological collapse. This instans is an ad-free, censorship-resistant video library with an account for every national group, hosted on renewable energy-powered infrastructure.',
          tags: [ '#ecology', '#collapsology', '#civil_disobedience' ],
          url: 'https://tube.rebellion.global/videos/local'
        },

        {
          type: 'instance',
          title: 'Blender',
          thumbnailName: 'blender.jpg',
          description: 'The Official Blender Foundation PeerTube instance give you access to videos presenting the evolutions of the 3D creation software, tutorials and animated films supported by the Blender Foundation. All videos published on this instance are under Creative Commons Attribution licence.',
          tags: [ '#Blender' ],
          url: 'https://video.blender.org/videos/local'
        },

        {
          type: 'video',
          title: 'The Hackers War',
          thumbnailName: 'hackers-war.png',
          description: 'The Hacker Wars (2014) is a film about the targeting of hacktivists and journalists by the US government. The film follows the information warriors who are fighting back, and it depicts the dangerous battle in which (h)ac(k)tivists fight for information freedom. Hacktivists impact the world in a new way by using the government\'s information against itself to call out those in power. The Hacker Wars takes you to the front lines of the high-stakes battle over the fate of the Internet, freedom and privacy.',
          tags: [ '#information', '#freedom', '#privacy' ],
          url: 'https://peertube.fr/videos/watch/161eaaa2-92ad-47f9-b6e0-17c7585bbef1'
        },

        {
          type: 'video',
          title: 'Missing Halloween',
          thumbnailName: 'missing-halloween.png',
          description: 'Written, directed, produced and animated by Mike Inel, Missing Halloween  is a short animation about a boy meeting his best friend to celebrate Halloween, making trick-or-treat. The Animation mashed sad stories with beautiful animation. The black and white drawings are beautiful and the story, although a little sad, is very poetic.',
          tags: [ '#Halloween', '#cute', '#friendship' ],
          url: 'https://vidcommons.org/videos/watch/4c9cece6-68dc-45be-a657-7118a5794123'
        },

        {
          type: 'channel',
          title: 'Techlore',
          thumbnailName: 'techlore.png',
          description: 'Techlore is Henry\'s channel, a tech enthusiast who\'s passionate about privacy and security. Started in december 2014, this channel contains many videos to spread awareness an offer a centralized place for information related to basic privacy and security while using the internet. With the video based-course Go Incognito, Henry aims to educate people around the world to transform their way of thinking about why, where, when and how to treat their data and personal information.',
          tags: [ '#privacy', '#data', '#security' ],
          url: 'https://bitcointv.com/c/techlore'
        },

        {
          type: 'channel',
          title: 'Copy-Me',
          thumbnailName: 'copy-me.png',
          description: 'Copy-Me is a media debunking the myths of copyright and copying. On this channel you can find 2 webseries. The Creativity Delusion is a multi-part video essay on how our misconceptions about ideas and the way brains work impact our views about creation. These also extend to intellectual property. And of course, this videos are under a free license, without any sort of restriction.',
          tags: [ '#creativity', '#publicdomain', '#sharing' ],
          url: 'https://tilvids.com/video-channels/copyme_channel/videos'
        },

        {
          type: 'channel',
          title: 'Gaby Weber Documentaries',
          thumbnailName: 'gaby-weber.png',
          description: 'Gabriele "Gaby" Weber is a German journalist. She has been reporting from South America since the mid-eighties, mainly for ARD. Her focal points are international politics, human rights and the history of German-Latin American relations. On this channel, you can discover her documentary movies in german, english and spanish languages.',
          tags: [ '#documentary', '#geopolitical', '#journalism' ],
          url: 'https://peertube.ch/accounts/gabyweber/video-channels'
        }
      ]
    }
  }
}
