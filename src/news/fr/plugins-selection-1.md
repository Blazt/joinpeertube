---
id: plugins-selection-1
title: Une sélection de plugins sur joinpeertube.org
date: March 11, 2021
---

<p>Bonjour,</p>
<p>En septembre 2019, à l'occasion de <a rel="noreferrer noopener" target="_blank"
    href="https://joinpeertube.org/fr/news#release-1-4-0">la sortie de la version 1.4 de PeerTube</a>, nous vous
  annoncions la mise en place d'un système de plugins. Ce système permet aux administrateur⋅ices d'instances PeerTube
  de créer et/ou d'installer des plugins, en fonction de leurs besoins spécifiques de fonctionnalités, sans faire
  reposer ce travail de création sur notre petite association.</p>
<p>Saviez-vous qu'il existe désormais plus de 40 plugins à disposition des
  administrateur⋅ices d'instances PeerTube ?</p>
<p>Afin d'encourager l'installation de ces plugins, nous venons de créer <a rel="noreferrer noopener" target="_blank"
    href="https://joinpeertube.org/plugins-selection">une nouvelle page</a>
  sur laquelle nous allons régulièrement mettre en valeur une sélection de plugins. Nous espérons ainsi que de plus en
  plus d'administrateur⋅ices d'instances les installent pour améliorer les fonctionnalités de leur instance.</p>
<p>Pour commencer, nous avons
  fait le choix de valoriser 4 plugins :</p>
<ul>
  <li><a rel="noreferrer noopener" target="_blank" href="/plugins-selection#video-annotation">video-annotation</a> pour
    ajouter des annotations sur une vidéo ;
  </li>
  <li><a rel="noreferrer noopener" target="_blank" href="/plugins-selection#upload-limits">upload-limits</a> pour alerter
    sur les limites de téléchargement ;</li>
  <li><a rel="noreferrer noopener" target="_blank" href="/plugins-selection#chapters">chapters</a> pour ajouter des
    chapitres à une vidéo ;</li>
  <li><a rel="noreferrer noopener" target="_blank" href="/plugins-selection#glavliiit">glavliiit</a> pour aider à la
    modération.</li>
</ul>
<p>Nous ajouterons régulièrement d'autres plugins à cette sélection. Bonne découverte !
</p>
