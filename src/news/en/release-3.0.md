---
id: release-3.0
title: PeerTube v3 is out, with p2p livestreaming!
date: January 7, 2021
---

<p>Hello,</p>

<p>
  It has been a very busy 6 months, but we have completed our roadmap and just released version 3 of PeerTube!
</p>

<ul>
  <li>
    Read all about it <a rel="noreferrer noopener" target="_blank"
      href="https://framablog.org/2021/01/07/peertube-v3-its-a-live-a-liiiiive/">on our blog</a>;
  </li>

  <li>
    See <a rel="noreferrer noopener" target="_blank"
      href="https://github.com/Chocobozzz/PeerTube/releases/tag/v3.0.0">the code release</a> by yourself;
  </li>

  <li>
    Watch the <a rel="noreferrer noopener" target="_blank"
      href="https://framatube.org/videos/watch/8519184e-b0c0-45f9-a005-4baddcd41f88">"PeerTube, Backstage"
      short-film</a> (in French, subtitles incoming).
  </li>
</ul>

<figure>
  <img loading="lazy"
    src="https://framablog.org/wp-content/uploads/2021/01/2020-12-11_peertube-V3_by-David-Revoy-1024x466.jpeg" alt="">
  <figcaption>Illustration CC-BY david revoy</figcaption>
</figure>

<p>
  We would like to thank every person who has contributed to the fundraising of the v3 for their generosity (especially
  in a difficult time for everyone). Thanks also to the sponsors of this v3, Octopuce, Code Lutin and the Debian
  project. Last but not least, our deepest thanks to every person who, in their own way, worked and contributed to help
  this v3 get complete.
</p>

<p>
  <translate>Happy free-libre live streams,</translate>
  <br />
  Framasoft.
</p>
