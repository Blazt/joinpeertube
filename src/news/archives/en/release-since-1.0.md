---
id: release-since-1.0-0
title: 'PeerTube: retrospective, new features and more to come!'
date: February 26, 2019
---

Since version 1.0 has been released last November, we went on improving PeerTube, day after day. These improvements on PeerTube go well beyond the objectives fixed during the crowdfunding. They have been funded by the [Framasoft non-profit](https://framasoft.org), which develops the software (and lives only through [your donations](https://soutenir.framasoft.org)).

Here is a small retrospective of the end of 2018/beginning of 2019:

In December 2018, we released version 1.1 which contained some moderation tools requested by instance administrators.

We also took the opportunity to add a watched videos history feature and the automatic resuming of video playback.</

In January, we released version 1.2 that supports 3 new languages: Russian, Polish and Italian. Thanks to PeerTube's community of translators, PeerTube is now translated into 16 different languages!

This version also includes a notification system that allows users to be informed (on the web interface or through email) when their video is commented, when someone mention them, when one of their subscriptions has published a new video, etc.

In the meantime, the PeerTube federation has grown: today, more than 300 instances broadcast more than 70,000 videos, with nearly 2 million cumulated views. We remind you that the only official website we maintain around PeerTube is https://joinpeertube.org/en and that we bear no responsibility on any other site that may be published.

As you can see, we have gone far beyond what the crowdfunding has funded. And we will continue!

For 2019, we plan to add a  plugin and theme management system (even though basic at first), playlist management, support for audio files upload and many other features.

If you also to contribute to the growing of PeerTube, you can participate in its funding here: https://soutenir.framasoft.org/en

If you have any questions, feel free to use our forum: https://framacolibri.org/c/peertube

Thanks to all PeerTube contributors!
Framasoft
