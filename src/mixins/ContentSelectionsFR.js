export default {
  data: function () {
    return {
      contentSelectionsFR: [
        {
          type: 'video',
          title: 'Nothing to hide',
          thumbnailName: 'nothing-to-hide.jpg',
          url: 'https://media.zat.im/videos/watch/35badfed-5322-48ac-b5c1-71b1ad88262e',
          tags: [ '#privacy', '#documentaire', '#capitalisme-de-surveillance' ],
          description: 'Nothing to Hide (2017) est un film documentaire franco-allemand de Marc Meillassoux et Mihaela Gladovic, qui s\'intéresse aux effets de la surveillance de masse sur les individus et la société. Proposant un regard critique à propos des lois sur le renseignement mises en place par de nombreux États ces dernières années, le film nous rappelle à quel point le débat sur l’usage des données personnelles est actuel et questionne les fondements de nos démocraties.'
        },

        {
          type: 'video',
          title: 'Démocratie(s)',
          thumbnailName: 'democraties.jpg',
          tags: [ '#democratie', '#politique', '#elections' ],
          description: 'Ce film d\'Henri Poulain, Julien Goetz et Sylvain Lapoix (les créateurs de la série DataGueule) met en évidence les travers de notre démocratie représentative et propose des  alternatives qui fonctionnent et dont nous pourrions nous inspirer. Objectif : explorer son passé pour mieux comprendre cette "crise démocratique" qui est sur toutes les lèvres.',
          url: 'https://peertube.datagueule.tv/videos/watch/0b04f13d-1e18-4f1d-814e-4979aa7c9c44'
        },

        {
          type: 'channel',
          title: 'DataGueule',
          thumbnailName: 'datagueule.jpg',
          description: 'DataGueule, c’est une websérie documentaire créée en 2014 qui présente à chaque épisode, de façon simple et imagée, une problématique sociétale à l’aide de données statistiques soigneusement sélectionnées et commentées (les datas). Un⋅e invité⋅e spécialiste du thème sélectionné enrichit de façon complémentaire l’argumentaire chiffré déjà solide, tout en proposant des solutions qui profiteraient au plus grand nombre. Un excellent moyen de se sensibiliser aux questions et problématiques qui secouent notre civilisation !',
          tags: [ '#DataJournalisme', '#documentaire', '#société' ],
          url: 'https://peertube.datagueule.tv/video-channels/c2fbac48-b069-42dd-b24c-7969c14a1374/videos'
        },

        {
          type: 'channel',
          title: 'Hygiène Mentale',
          thumbnailName: 'hygiene-mentale.jpg',
          description: 'La chaîne Hygiène Mentale se donne pour objectif de nous apprendre à nous forger un esprit critique face aux flux permanents d’informations qui prétendent tous détenir les clés de la vérité. En nous dispensant divers conseils basiques pour ne pas nous laisser tromper par des arguments fallacieux, Christophe Michel (le créateur de la chaîne) nous permet de développer des méthodes d\'autodéfense intellectuelle et de nous faire découvrir les instruments critiques pour nous protéger de la désinformation.',
          tags: [ '#VulgarisationScientifique', '#zététique', '#désinformation' ],
          url: 'https://skeptikon.fr/video-channels/ecf58044-ecfd-46b8-bf6f-d8206bbace38/videos'
        },

        {
          type: 'instance',
          title: 'Académie de Lyon',
          thumbnailName: 'academie-lyon.jpg',
          description: 'L\'instance de l\'Académie de Lyon propose à tous les enseignants des établissements scolaires du territoire d\'héberger les vidéos produites lors de leurs activités pédagogiques. On y trouve donc des vidéos sur des thèmes aussi variés que les mathématiques, l\'informatique et les enjeux du numérique,  les méthodes pédagogiques, la coiffure ou la cuisine. Et les formes aussi sont très variables : tutoriels, captations, reportages, témoignages, interviews, etc.',
          tags: [ '#enseignement', '#pédagogie', '#tutoriel' ],
          url: 'https://tube.ac-lyon.fr/'
        },

        {
          type: 'instance',
          title: 'Colibri Outils Libres',
          thumbnailName: 'colibris.jpg',
          description: 'Cette instance, maintenue et modérée par l\'association Colibris, diffuse des vidéos sur les thèmes de la transition écologique. On y retrouve les nombreuses vidéos utilisées dans les différents Moocs créés par l\'association, des reportages et documentaires sur la nature et l\'écologie, mais aussi des vidéos de Jean-Michel Cornu de la chaîne Trucs d\'animation sur les techniques d\'animation et de facilitation.',
          tags: [ '#écologie', '#permaculture', '#monnaies' ],
          url: 'https://video.colibris-outilslibres.org/videos/local'
        },

        {
          type: 'video',
          title: 'Big Buck Bunny',
          thumbnailName: 'bunny.jpg',
          description: 'Big Buck Bunny raconte l\'histoire d\'un lapin géant avec un cœur gros comme ça. Mais lorsque trois rongeurs malicieux le harcèlent rudement et font preuve de cruauté envers les autres animaux, quelque chose se déclenche chez lui... Dans la tradition typique des dessins animés, il prépare aux méchants rongeurs une vengeance comique. Ce court-métrage d\'animation s\'inscrit dans la ligne directe des réalisations proposées par la Fondation Blender sous le nom de code "projet pêche".',
          tags: [ '#lapin', '#vengeance', '#animation' ],
          url: 'https://video.blender.org/videos/watch/bf1f3fb5-b119-4f9f-9930-8e20e892b898'
        },

        {
          type: 'video',
          title: 'En quête d\'autonomie',
          thumbnailName: 'autonomie.jpg',
          description: 'Notre génération va devoir faire face à un effondrement économique, politique et biotique. Il devient donc impératif de faire société autrement et, surtout, d’apprendre à être résilient. Ce documentaire réalisé par Demos Kratos et entièrement auto-produit grâce au financement participatif va à la rencontre des habitants de trois écovillages pour apprendre de leurs expériences. Au travers de leurs témoignages, ils nous transmettent les enseignements qu’ils ont tirés pour bâtir leurs habitats, produire leur énergie, faire pousser leur alimentation, s’organiser en collectif et ainsi devenir plus autonomes.',
          tags: [ '#ecovillage', '#autonomie', '#activisme' ],
          url: 'https://tube.nuagelibre.fr/videos/watch/128030c5-f548-47d6-8379-fb436cea665f'
        },

        {
          type: 'channel',
          thumbnailName: 'stop-nous.jpg',
          title: 'Stop nous si tu peux',
          description: 'Une chaîne pour sensibiliser au voyage alternatif et aux pratiques de l\'auto-stop. Le principe ? Voyager sobrement, sans argent, en favorisant les rencontres, les échanges et l\'entraide ! Équipés d\'un sac à dos, de caméras et d’une dose de bonne humeur, découvrez les aventures de Thomas et Quentin, deux baroudeurs qui misent sur la bienveillance pour réussir à se loger, se déplacer et se nourrir.',
          tags: [ '#auto-stop', '#voyage', '#liberté' ],
          url: 'https://hitchtube.fr/video-channels/snstp_channel/videos'
        },

        {
          type: 'instance',
          thumbnailName: 'extinction-rebellion.jpg',
          title: 'Extinction Tubellion',
          description: 'Extinction Tubellion est l\'instance Peertube du mouvement Extinction Rebellion France. Retrouvez sur cette instance de nombreuses vidéos documentant les actions de ce mouvement social écologiste qui revendique l\'usage de la désobéissance civile non violente afin d\'inciter les gouvernements à agir dans le but d\'éviter les points de basculement dans le système climatique, la perte de la biodiversité et le risque d\'effondrement social et écologique.',
          tags: [ '#écologie', '#collapsologie', '#désobéissance_civile' ],
          url: 'https://tube.extinctionrebellion.fr/'
        },

        {
          type: 'instance',
          thumbnailName: 'default.jpg',
          title: 'Qu\'est-ce que tu GEEKes ?',
          description: 'Qu’est-ce que tu GEEKes ? (ou QTG? en abrégé) a un seul objectif : vulgariser l’informatique pour la rendre compréhensible par tous avec des réalisations sourcées, libres de droit et sans publicité. Et pour réaliser cela, Tom et Flo nous proposent des vidéos regroupées sous 5 formats différents, histoire qu\'il y en ait pour tous les goûts. L’ensemble des réalisations sont libres de droit d’exploitation et de modification et ne contiennent pas de publicité.',
          tags: [ '#informatique', '#vulgarisation', '#tutoriel' ],
          url: 'https://peertube.qtg.fr/'
        },

        {
          type: 'video',
          thumbnailName: 'confi-manu.png',
          title: 'Confi-Conseils d\'Emmanuel Revah',
          description: 'Cette vidéo publiée en pleine période de confinement nous donne une petite liste de conseils pour bien vivre cette situation exceptionnelle. Avec beaucoup d\'humour, Emmanuel Revah partage sa vision du confinement.',
          tags: [ '#humour', '#confinement', '#covid19' ],
          url: 'https://tube.hoga.fr/videos/watch/9cc59aaa-20fc-4c74-aa02-c28e76369260'
        },

        {
          type: 'channel',
          thumbnailName: 'numericatous.png',
          title: 'Numericatous, le numérique pour tous',
          description: 'Cette chaîne vous propose plus de 20 vidéos pour vous accompagner et vous initier à l’usage d’un numérique libre et éthique. Découvrez les principaux logiciels libres et leur fonctionnement ainsi que de nombreuses astuces pour modifier vos pratiques numériques.',
          tags: [ '#tutoriel', '#logiciels_libres' ],
          url: 'https://peertube.iriseden.eu/video-channels/numericatous_linux/videos'
        },

        {
          type: 'channel',
          thumbnailName: 'heureka.png',
          title: 'Heu?reka',
          description: 'A l\'aide de nombreux schémas, graphiques et animations pour rendre ses explications plus intelligibles, Gilles Mitteau explique depuis 2015 avec pédagogie et humour le fonctionnement de l\'économie et de la finance, en particulier leurs mécanismes les plus complexes et les plus austères. Les épisodes sont écrits comme des dialogues entre deux personnages afin de permettre une meilleure compréhension.',
          tags: [ '#économie', '#finance', '#VulgarisationScientifique' ],
          url: 'https://indymotion.fr/video-channels/heu.reka_channel/videos'
        },

        {
          type: 'instance',
          thumbnailName: 'skeptikon.png',
          title: 'Skeptikón',
          description: 'Association à but non-lucratif, Skeptikón fait la promotion de l\'esprit critique et de la démarche scientifique, notamment par la production et la mise à disposition de ressources textuelles ou audiovisuelles dédiées à ces sujets. Leur instance regroupe à ce jour plus de 900 vidéos dédiées à la zététique, à l\'esprit critique et au scepticisme de manière plus générale. Si ces sujets vous intéressent, vous y trouverez, littéralement, des heures entières de vidéos portant sur des sujets divers.',
          tags: [ '#zététique', '#scepticisme', '#VulgarisationScientifique' ],
          url: 'https://skeptikon.fr/videos/local'
        },

        {
          type: 'video',
          thumbnailName: 'bigpharma.png',
          title: 'Vers une santé à plusieurs vitesses avec Big Pharma et ses brevets',
          description: 'Ce bonus du film documentaire de Philippe Borrel "La bataille du Libre" est l\'interview d\'Olivier Maguet (juriste / administrateur bénévole à Médecins du Monde), du Pr Philippe Even (médecin pneumologue) et de la chercheuse en sciences sociales Gaëlle Krikorian. On y découvre comment l\'industrie pharmaceutique s\'est concentrée sur les trente dernières années, comment les logiques spéculatives ont pervertit la propriété intellectuelle. Et on comprend comment les laboratoires pharmaceutiques arrivent à vendre un produit non pas en fonction du coût de recherche et de développement, mais uniquement en fonction de ce que les malades (ou les Etats) sont prêt à payer.',
          tags: [ '#BigPharma', '#brevets', '#santé' ],
          url: 'https://peertube.fr/videos/watch/6d4f9b37-8417-465f-8149-5d0712216dd7'
        },

        {
          type: 'channel',
          thumbnailName: 'monsieurphi.png',
          title: 'Monsieur Phi',
          description: 'Depuis 4 ans, sur cette chaîne, MonsieurPhi (Thibaut Giraud, un prof de philo devenu vidéaste) nous parle de ce qu\'est la philosophie, de son utilité, de son rapport aux sciences et à la religion, de philosophes, d’écrivains, de fascinants théorèmes et même de l’éventuelle probabilité que nous soyons tous des simulations. Des thèmes compliqués en apparence simplifiés pour les rendre audibles et compréhensibles pour les non-initiés !',
          tags: [ '#philosophie', '#science', '#VulgarisationScientifique' ],
          url: 'https://video.antopie.org/video-channels/monsieurphi/videos'
        },

        {
          type: 'instance',
          thumbnailName: 'pair2jeux.png',
          title: 'P2J : Pair 2 jeux',
          description: 'L\'instance Pair 2 jeux regroupe principalement des créations de vidéastes français passionnés par les jeux vidéo. Hébergeant plus de 2800 vidéos, Pair 2 jeux vous permettra de découvrir l\'univers des jeux vidéo, que ce soit via des vidéos de présentation, des extraits ou des séquences live de gameplay. Victime de son succès, les inscriptions pour uploader des vidéos sont fermées pour le moment, mais vous pouvez toujours vous inscrire pour suivre et commenter celles qui y sont diffusées !',
          tags: [ '#jeuxvidéo', '#gameplay' ],
          url: 'https://videos.pair2jeux.tube/'
        }
      ]
    }
  }
}
