---
id: roadmap-v3
title: Roadmap to PeerTube's v3
date: May 27, 2020
mastodon: https://framapiaf.org/@peertube/104235499771402074
twitter: https://twitter.com/joinpeertube/status/1265305066577235969
---

Yesterday, we've published our [new roadmap to PeerTube's v3](https://joinpeertube.org/roadmap) detailing key features such as:

- global search
- moderation tools
- plugin & playlists
- peer-to-peer live streaming

Please read [our blogpost](https://framablog.org/2020/05/26/our-plans-for-peertube-v3-progressive-fundraising-live-streaming-coming-next-fall) to learn more about our choices for the next 6 months of development.

This PeerTube v3 should be published on november, 2020.

In the meantime,we hope you will [share and support this new JoinPeertube roadmap](https://joinpeertube.org/roadmap/).

Freely,
Framasoft
